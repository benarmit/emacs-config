;;----- MELPA Packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
;;-----

;; Automated package selection
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8ca8fbaeaeff06ac803d7c42de1430b9765d22a439efc45b5ac572c2d9d09b16" default))
 '(delete-selection-mode t)
 '(package-selected-packages
   '(ob-go which-key doom-modeline-mode doom-modeline use-package elpher slime dockerfile-mode go-mode yaml-mode magit company paredit geiser humanoid-themes)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Setup use-package
(package-install 'use-package)
(require 'use-package)
(setq use-package-always-ensure t) ; Downloads packages on first use if they don't exist

;; Automated package setup and install
(use-package company)
(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom (doom-modeline-height 15))
(use-package elpher)
(use-package geiser)
(use-package go-mode)
(use-package humanoid-themes)
(use-package magit)
(use-package paredit)
(use-package slime)
(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config (setq which-key-idle-delay 0.5)
  :pin "melpa")
(use-package yaml-mode)
(use-package ob-go)

;; Clear up GUI elements
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(setq inhibit-startup-screen t)
;; Visually wrap lines longer than frame
(visual-line-mode 1)

;; Use Humanoid Dark theme
(load-theme 'humanoid-dark)
(add-to-list 'default-frame-alist
	     '(font . "FiraCode Medium-10"))

;; Display time on the command bar
(display-time-mode 1)

;; Enable deleting of selected regions
(delete-selection-mode 1)

;; Setup directory for emacs backup files
(setq backup-directory-alist `(("." . "~/.saves")))

;; Setup SBCL (lisp compiler) for Slime
(setq inferior-lisp-program "/usr/bin/sbcl")

;; Geiser setup
(setq geiser-guile-binary "/usr/bin/guile2.2")

;; Enable language evaluation for Org mode
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (lisp . t)
   (scheme . t)
   (go . t)
   (shell . t)))
